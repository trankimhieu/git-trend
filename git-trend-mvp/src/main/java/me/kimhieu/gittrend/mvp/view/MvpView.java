package me.kimhieu.gittrend.mvp.view;

import android.content.Context;

public interface MvpView {

    Context getContext();
}
