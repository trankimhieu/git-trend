package me.kimhieu.gittrend.mvp.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import me.kimhieu.gittrend.mvp.R;
import me.kimhieu.gittrend.mvp.TrendingAdapter;
import me.kimhieu.gittrend.mvp.model.TrendingRepository;
import me.kimhieu.gittrend.mvp.presenter.TrendingPresenter;
import me.kimhieu.gittrend.mvp.presenter.TrendingRepositoryPresenter;

/**
 * Created by hieutran on 24/02/2016.
 */
public class TrendingActivity extends AppCompatActivity implements TrendingMvpView {

    private Toolbar toolbar;
    private TrendingPresenter presenter;
    private List<String> languageList;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ProgressDialog mProgressDialog;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.activity_trending);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        presenter = new TrendingPresenter();
        presenter.attachView(this);
        presenter.loadLanguages();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLanguageTabs(List<String> languages) {
        languages.add(0,"All Language");
        this.languageList = languages;
        setupSectionsPager();
        mProgressDialog.hide();
    }

    private void setupSectionsPager() {
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
    }

    @Override
    public void showMessage(int stringId) {

    }

    @Override
    public void showProgressIndicator() {
        if(mProgressDialog == null){
            mProgressDialog = new ProgressDialog(getContext());
        }
        mProgressDialog.setMessage("Loading");
        mProgressDialog.show();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment implements TrendingRepositoryMvpView {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_LANGUAGE_NAME = "language_name";

        RecyclerView mRecyclerView;
        private TrendingAdapter mAdapter;
        private RecyclerView.LayoutManager mLayoutManager;
        private TrendingRepositoryPresenter presenter;

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(String languageName) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putString(ARG_LANGUAGE_NAME, languageName);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_trending, container, false);

            presenter = new TrendingRepositoryPresenter();
            presenter.attachView(this);

            mRecyclerView = (RecyclerView) rootView.findViewById(R.id.repos_recycler_view);
            mRecyclerView.setHasFixedSize(true);

            // use a linear layout manager
            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mAdapter = new TrendingAdapter();
            mRecyclerView.setAdapter(mAdapter);
            String languageName = getArguments().getString(ARG_LANGUAGE_NAME);
            presenter.loadRepositories(languageName);
            return rootView;
        }

        @Override
        public void showRepositories(List<TrendingRepository> repositories) {
            mAdapter.setRepositories(repositories);
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public void showMessage(int stringId) {

        }

        @Override
        public void showProgressIndicator() {

        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            String languageName = "";
            if(position == 0){
                languageName = null;
            } else {
                languageName = languageList.get(position);
            }
            return PlaceholderFragment.newInstance(languageName);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return languageList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return languageList.get(position);
        }
    }
}
