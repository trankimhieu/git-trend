package me.kimhieu.gittrend.mvp.view;

import java.util.List;

import me.kimhieu.gittrend.mvp.model.Repository;

public interface MainMvpView extends MvpView {

    void showRepositories(List<Repository> repositories);

    void showMessage(int stringId);

    void showProgressIndicator();
}
