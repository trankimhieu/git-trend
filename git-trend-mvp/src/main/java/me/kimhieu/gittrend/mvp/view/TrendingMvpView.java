package me.kimhieu.gittrend.mvp.view;

import java.util.List;

import me.kimhieu.gittrend.mvp.model.Repository;
import me.kimhieu.gittrend.mvp.model.Trending;
import me.kimhieu.gittrend.mvp.model.TrendingRepository;

/**
 * Created by hieutran on 24/02/2016.
 */
public interface TrendingMvpView extends MvpView {

    void showLanguageTabs(List<String> languages);

    void showMessage(int stringId);

    void showProgressIndicator();

}
