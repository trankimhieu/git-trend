package me.kimhieu.gittrend.mvp.presenter;

import android.util.Log;

import java.util.List;

import me.kimhieu.gittrend.mvp.ArchiApplication;
import me.kimhieu.gittrend.mvp.R;
import me.kimhieu.gittrend.mvp.model.GithubTrendingService;
import me.kimhieu.gittrend.mvp.model.Languages;
import me.kimhieu.gittrend.mvp.model.Repository;
import me.kimhieu.gittrend.mvp.model.Trending;
import me.kimhieu.gittrend.mvp.model.TrendingRepository;
import me.kimhieu.gittrend.mvp.view.MainMvpView;
import me.kimhieu.gittrend.mvp.view.TrendingMvpView;
import retrofit.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by hieutran on 24/02/2016.
 */
public class TrendingPresenter implements Presenter<TrendingMvpView> {

    private TrendingMvpView trendingMvpView;
    private Subscription subscription;
    private List<String> languages;
    public static String TAG = "TrendingPresenter";

    @Override
    public void attachView(TrendingMvpView view) {
        this.trendingMvpView = view;
    }

    @Override
    public void detachView() {
        this.trendingMvpView = null;
        if(subscription != null) subscription.unsubscribe();
    }

    public void loadLanguages(){
        if(subscription !=null) subscription.unsubscribe();
        trendingMvpView.showProgressIndicator();
        ArchiApplication application = ArchiApplication.get(trendingMvpView.getContext());
        GithubTrendingService githubTrendingService = application.getGithubTrendingService();
        subscription = githubTrendingService.trendingLanguages()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(new Subscriber<Languages>() {
                    @Override
                    public void onCompleted() {
                        if(!languages.isEmpty()){
                            trendingMvpView.showLanguageTabs(languages);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Languages languages) {
                        TrendingPresenter.this.languages = languages.getData().getLanguages();
                    }
                });
    }

    private static boolean isHttp404(Throwable error) {
        return error instanceof HttpException && ((HttpException) error).code() == 404;
    }
}
