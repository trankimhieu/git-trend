package me.kimhieu.gittrend.mvp.view;

import java.util.List;

import me.kimhieu.gittrend.mvp.model.TrendingRepository;

/**
 * Created by hieutran on 24/02/2016.
 */
public interface TrendingRepositoryMvpView extends MvpView {

    void showRepositories(List<TrendingRepository> repositories);

    void showMessage(int stringId);

    void showProgressIndicator();
}
