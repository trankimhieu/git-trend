package me.kimhieu.gittrend.mvp.view;

import me.kimhieu.gittrend.mvp.model.User;

public interface RepositoryMvpView extends MvpView {

    void showOwner(final User owner);

}
