package me.kimhieu.gittrend.mvp.presenter;

import android.util.Log;

import java.util.List;

import me.kimhieu.gittrend.mvp.ArchiApplication;
import me.kimhieu.gittrend.mvp.R;
import me.kimhieu.gittrend.mvp.model.GithubTrendingService;
import me.kimhieu.gittrend.mvp.model.Trending;
import me.kimhieu.gittrend.mvp.model.TrendingRepository;
import me.kimhieu.gittrend.mvp.view.TrendingMvpView;
import me.kimhieu.gittrend.mvp.view.TrendingRepositoryMvpView;
import retrofit.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by hieutran on 24/02/2016.
 */
public class TrendingRepositoryPresenter implements Presenter<TrendingRepositoryMvpView> {
    private TrendingRepositoryMvpView trendingRepositoryMvpView;
    private Subscription subscription;
    private List<TrendingRepository> repositories;
    public static String TAG = "TrendingRepositoryPresenter";

    @Override
    public void attachView(TrendingRepositoryMvpView view) {
        this.trendingRepositoryMvpView = view;
    }

    @Override
    public void detachView() {
        this.trendingRepositoryMvpView = null;
        if(subscription != null) subscription.unsubscribe();
    }


    public void loadRepositories(String languageName){
        if(subscription != null) subscription.unsubscribe();
        ArchiApplication application = ArchiApplication.get(trendingRepositoryMvpView.getContext());
        GithubTrendingService githubTrendingService = application.getGithubTrendingService();
        subscription = githubTrendingService.trendingRepositories(languageName)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(new Subscriber<Trending>() {
                    @Override
                    public void onCompleted() {
                        Log.i(TAG,"Repos loaded" + repositories);
                        if(!repositories.isEmpty()){
                            trendingRepositoryMvpView.showRepositories(repositories);
                        } else {
                            trendingRepositoryMvpView.showMessage(R.string.error_loading_repos);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "Error loading Github Trending", e);
                        if(isHttp404(e)){
                            trendingRepositoryMvpView.showMessage(R.string.error_username_not_found);
                        } else {
                            trendingRepositoryMvpView.showMessage(R.string.error_loading_repos);
                        }
                    }

                    @Override
                    public void onNext(Trending trending) {
                        TrendingRepositoryPresenter.this.repositories = trending.getData().getRepositories();
                    }
                });

    }
    private static boolean isHttp404(Throwable error) {
        return error instanceof HttpException && ((HttpException) error).code() == 404;
    }
}
