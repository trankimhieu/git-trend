package me.kimhieu.gittrend.mvc;

import android.app.ProgressDialog;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import me.kimhieu.gittrend.mvc.model.GithubService;
import me.kimhieu.gittrend.mvc.model.GithubTrendingService;
import me.kimhieu.gittrend.mvc.model.Languages;
import me.kimhieu.gittrend.mvc.model.Trending;
import me.kimhieu.gittrend.mvc.model.User;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class TrendingActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    private ProgressDialog mProgressDialog;
    private Subscription subscription;
    private final String TAG = "Trending Activity";
    private List<String> listLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trending);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


//        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
//        tabLayout.setupWithViewPager(mViewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        mProgressDialog = new ProgressDialog(TrendingActivity.this);
        mProgressDialog.setMessage("Loading");
        mProgressDialog.show();
        loadLanguagesFromGitHubTrendingAPI();

    }

    private void loadLanguagesFromGitHubTrendingAPI(){
        ArchiApplication application = ArchiApplication.get(this);
        GithubTrendingService githubTrendingService = application.getGithubTrendingService();
        Observable<Languages> languages = githubTrendingService.trendingLanguages();
         languages.subscribeOn(Schedulers.io())
                 .observeOn(AndroidSchedulers.mainThread())
                 .unsubscribeOn(Schedulers.io())
                 .subscribe
                 (new Subscriber<Languages>() {
                     @Override
                     public void onCompleted() {

                     }

                     @Override
                     public void onError(Throwable e) {

                     }

                     @Override
                     public void onNext(Languages languages) {
                         Log.d("Status", languages.getStatus());
                         listLanguage = languages.getData().getLanguages();
                         listLanguage.add(0,"All Language");
                         setupSectionsPager();
                         mProgressDialog.hide();
                     }
                 });
    }


    private void setupSectionsPager(){
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_trending, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_LANGUAGE_NAME = "language_name";

        RecyclerView mRecyclerView;
        private TrendingAdapter mAdapter;
        private RecyclerView.LayoutManager mLayoutManager;

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(String languageName) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putString(ARG_LANGUAGE_NAME, languageName);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_trending, container, false);

            mRecyclerView = (RecyclerView) rootView.findViewById(R.id.repos_recycler_view);
            mRecyclerView.setHasFixedSize(true);

            // use a linear layout manager
            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mAdapter = new TrendingAdapter();
            mRecyclerView.setAdapter(mAdapter);
            String languageName = getArguments().getString(ARG_LANGUAGE_NAME);
            loadDataToRecylerView(languageName);
            return rootView;
        }

        private void loadDataToRecylerView(String languageName) {
            ArchiApplication application = ArchiApplication.get(getActivity());
            GithubTrendingService githubTrendingService = application.getGithubTrendingService();
            Observable<Trending> trending = githubTrendingService.trendingRepositories(languageName);
            trending.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .subscribe
                            (new Subscriber<Trending>() {
                                @Override
                                public void onCompleted() {

                                }

                                @Override
                                public void onError(Throwable e) {

                                }

                                @Override
                                public void onNext(Trending trending) {
                                    Log.d("Status", trending.getStatus());
                                    // use this setting to improve performance if you know that changes
                                    // in content do not change the layout size of the RecyclerView
                                    // specify an adapter (see also next example)
                                    mAdapter.setRepositories(trending.getData().getRepositories());
                                    mAdapter.notifyDataSetChanged();

                                }
                            });
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            String languageName = "";
            if(position == 0){
                languageName = null;
            } else {
                languageName = listLanguage.get(position);
            }
            return PlaceholderFragment.newInstance(languageName);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return listLanguage.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return listLanguage.get(position);
        }
    }
}
