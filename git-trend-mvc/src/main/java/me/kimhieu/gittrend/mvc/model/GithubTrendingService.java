package me.kimhieu.gittrend.mvc.model;

import java.util.List;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.http.Url;
import rx.Observable;

public interface GithubTrendingService {

    @GET("api/v1/languages.json")
    Observable<Languages> trendingLanguages();

    @GET("api/v1/trending.json")
    Observable<Trending> trendingRepositories(@Query("language") String language);

    class Factory {
        public static GithubTrendingService create() {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://github-trending.herokuapp.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
            return retrofit.create(GithubTrendingService.class);
        }
    }
}
