
package me.kimhieu.gittrend.mvc.model;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class TrendingData {

    @SerializedName("repositories")
    @Expose
    private List<TrendingRepository> repositories = new ArrayList<TrendingRepository>();

    /**
     * 
     * @return
     *     The repositories
     */
    public List<TrendingRepository> getRepositories() {
        return repositories;
    }

    /**
     * 
     * @param repositories
     *     The repositories
     */
    public void setRepositories(List<TrendingRepository> repositories) {
        this.repositories = repositories;
    }

}
