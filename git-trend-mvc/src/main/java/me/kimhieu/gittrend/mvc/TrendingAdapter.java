package me.kimhieu.gittrend.mvc;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import me.kimhieu.gittrend.mvc.model.Repository;
import me.kimhieu.gittrend.mvc.model.Trending;
import me.kimhieu.gittrend.mvc.model.TrendingRepository;

/**
 * Created by kimhieu on 2/24/16.
 */
public class TrendingAdapter extends RecyclerView.Adapter<TrendingAdapter.TrendingViewHolder> {


    private List<TrendingRepository> repositories;
    private Callback callback;

    public TrendingAdapter() {
        this.repositories = Collections.emptyList();
    }

    public TrendingAdapter(List<TrendingRepository> repositories) {
        this.repositories = repositories;
    }

    public void setRepositories(List<TrendingRepository> repositories) {
        this.repositories = repositories;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public TrendingAdapter.TrendingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_trending_repo, parent, false);
        final TrendingViewHolder viewHolder = new TrendingViewHolder(itemView);
        viewHolder.contentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.onItemClick(viewHolder.repository);
                }
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TrendingAdapter.TrendingViewHolder holder, int position) {
        TrendingRepository repository = repositories.get(position);
        Context context = holder.titleTextView.getContext();
        holder.repository = repository;
        holder.titleTextView.setText(repository.getName());
        holder.descriptionTextView.setText(repository.getDescription());
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    public static class TrendingViewHolder extends RecyclerView.ViewHolder {
        public View contentLayout;
        public TextView titleTextView;
        public TextView descriptionTextView;
        public TrendingRepository repository;

        public TrendingViewHolder(View itemView) {
            super(itemView);
            contentLayout = itemView.findViewById(R.id.layout_content);
            titleTextView = (TextView) itemView.findViewById(R.id.text_repo_title);
            descriptionTextView = (TextView) itemView.findViewById(R.id.text_repo_description);
        }
    }

    public interface Callback {
        void onItemClick(TrendingRepository repository);
    }
}
