package me.kimhieu.gittrend.mvvm.viewmodel;

/**
 * Interface that every ViewModel must implement
 */
public interface ViewModel {

    void destroy();
}
